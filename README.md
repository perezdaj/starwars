# StarWars

StarWars is an introduction to hybrid mobile applications

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 
The .net core web api is in the \StarWars.API folder and the Ionic app is in the \StarWars.SPA folder. In the demo I am using the existing StarWars.API and creating a new StarWars-SPA to show how to create and build the Ionic UI from begining to end.

### Prerequisites to install

Install DotNetCore SDK
```
https://dotnet.microsoft.com/download
```

Install Git
```
https://git-scm.com/downloads
```

Install Node/NPM
```
https://nodejs.org/en/download/
```
Install Ionic after you have Node/NPM installed
```
npm install -g @ionic/cli
```

Install VS Code
```
https://code.visualstudio.com/download
```

Add extensions to VS Code
```
C# for Visual Studio Code (powered by OmniSharp)
Debugger for Chrome
vscode-icons
```

### Testing your installs

Open terminal or command line

Test git
```
c:\code>git --version
git version 2.26.2.windows.1
```

Test DotNetCore
```
c:\code>dotnet --version
3.1.201
```

Test Node/NPM
```
c:\code>node --version
v12.16.2

c:\code>npm --version
6.14.4
```

Test Ionic
```
PEREZDAJ@AI935245 MINGW64 /c/code/starwars/starwars.spa (master)
$ ionic --version
6.9.2
```

## Cloning the StarWars Project

Open terminal or command line

Move to the folder where you want to copy the project
```
C:\WINDOWS\system32>cd c:/code
```

Clone the project
```
c:\code>git clone https://perezdaj@bitbucket.org/perezdaj/starwars.git
```

### Open the project

Move to the project folder
```
c:\code>cd starwars
c:\code\starwars>
```

Open the project folder in VS Code
```
c:\code\starwars>code .
```

### Running the App

In VS Code after you have opened the project folder c:\code\starwars. Open a New Terminal in VS Code Ctrl+Shift+`

## Start Web API

Move to the StarWars.API folder
```
PEREZDAJ@AI935245 MINGW64 /c/code/starwars (master)
$ cd starwars.api
```

Run dotnet web api
```
PEREZDAJ@AI935245 MINGW64 /c/code/starwars/starwars.api (master)
$ dotnet watch run
```

Test to make sure you are getting data back by browsing to:
```
http://localhost:5000/starwars
```
or you can use a tool like Postman

## Start Ionic SPA

Run npm install to install all dependancies
```
PEREZDAJ@AI935245 MINGW64 /c/code/starwars/starwars.spa (master)
$ npm install
```
This should install Ionic and everything else needed

Install Ionic if you have not already
```
npm install -g @ionic/cli
```

Open a New Terminal in VS Code Ctrl+Shift+`

Move to the StarWars.API folder
```
PEREZDAJ@AI935245 MINGW64 /c/code/starwars (master)
$ cd starwars.spa
```

Run Ionic CLI Server
```
PEREZDAJ@AI935245 MINGW64 /c/code/starwars/starwars.spa (master)
$ ionic serve
```

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Darren Perez** - *Initial work* - [StarWars](https://bitbucket.org/perezdaj/starwars/src/master/)


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

